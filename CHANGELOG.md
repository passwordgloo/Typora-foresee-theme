# CHANGELOG

## 0.0.5.1（2025-02-15）

Readme: Add CHANGELOG and demo

## 0.0.4.8-0.0.5.0

Readme: Update image link

### Features

Theme: Rounded corner style, add title 3 buffer animation

### Added

Readme: Change presentation images and standardize Readme documents

## 0.0.4.7(2024-12-05)

Theme: Modify the title style using Mac style and change the h2 color block to gradient color

## 0.0.4.6(2024-11-30)

Format: Beautify the code style

Theme: Change the title focus mode style and add an animated transition effect

## 0.0.4.5(2024-11-26)

Theme: Fixed sidebar color issues

## 0.0.4.4(2024-11-22)

Readme: modifies the GitHub style block type

## 0.0.4.3(2024-11-22)

Readme: Beautify the original prompt with GitHub style code blocks

Theme: Change the gradient red

## 0.0.4.2(2024-10-07)

### Feature

Theme: Mac style code style

Style: Gradient color blocks are richer

### Preview

![](https://img.picgo.net/2024/10/07/PixPin_2024-10-07_10-52-467f14386bb44c147e.webp)

![](https://img.picgo.net/2024/10/07/PixPin_2024-10-07_11-06-58b1385e0e76a87061.webp)

## 0.0.4.1(2024-10-06)

Style: Updated version number

## 0.0.4.0(2024-10-06)

Readme: Update image link

## 0.0.3.9(2024-08-18)

Readme: Update image link

## 0.0.3.8(2024-08-18)

Readme: Update image link

## 0.0.3.7(2024-08-18)

### Feature

Theme: Adds a GitHub style reference block

## 0.0.3.6(2024-08-18)

### Remove

Theme: Dark and light mode files

Test: Tutorial documentation

### Feature

Theme: A theme file to automatically switch between dark and light themes

Readme: Demonstrate theme styles using Mac style

### Preview

![](https://img.picgo.net/2024/08/18/PixPin_2024-08-18_17-20-320212612d5f2cd57c.png)

![](https://img.picgo.net/2024/08/18/PixPin_2024-08-18_17-17-044fd017cfb6a0a347.png)

![](https://img.picgo.net/2024/08/18/PixPin_2024-08-18_17-19-26d719e5755cc35953.png)

## 0.0.3.6(2024-08-18)

Readme: Demonstrates theme styles using Mac style

## 0.0.3.5(2024-08-16)

Theme: Added public gradients, new styles with icon reference bars, new ICONS

Test: Demonstrates the reference bar effect

## 0.0.3.4(2024-07-31)

Format: Rename the style file

## 0.0.3.3(2024-07-30)

Theme: Modifies the block background and cursor color

## 0.0.3.2(2024-07-30)

Format: Rename the icon file

Test: deletes the English tutorial file

## 0.0.3.1(2024-07-30)

### 修改

Format: Use the map function to select a color

Readme: Introduction to modifying custom code

## 0.0.3.0(2024-07-27)

Format: Split the theme scss into two files, light and dark, and merge the code files into the corresponding mode files

Theme: Modify the block style

## 0.0.2.9(2024-07-27)

Readme: Modify the presentation images used in the introduction page

## 0.0.2.8(2024-07-24)

Readme: Modify the introduction page

## 0.0.2.7(2024-05-27)

Readme: Deprecates oss image loading speed

## 0.0.2.6(2024-05-03)

Readme: Add the language used by the code block example to speed up image loading using oss

## 0.0.2.5(2024-05-03)

### Modify

Readme: Add code block examples to improve image loading speed using picgo chart bed

### Added

Theme: Compress the code style to code.min.csss

### Feature

Theme: Reference block supports widescreen gradient color blocks and color-changing ICONS

## 0.0.2.4(2024-05-03)

Readme: Modify the introduction page

## 0.0.2.3(2024-05-03)

Readme: Modify the introduction page

## 0.0.2.3(2024-05-01)

Readme: Modify the introduction page

Theme: Underline style changed

## 0.0.2.2(2024-02-25)

### Modify

Readme: Add new feature introduction

### Added

Test: Adds English tutorial documentation

Style: Added `badge` folder

### Feature

Adjust the title style and add a style in Focus mode

## 0.0.2.1(2024-02-24)

Format: Use the common style to modify the reference block, badge style

## 0.0.2.0(2024-02-23)

Theme: Modified h2, h3, h5 styles

## 0.0.1.9(2024-02-23)

Format: Modify the scss common style using @mixin and @include syntax

## 0.0.1.8(2024-02-21)

Readme: Modify the address of the demo image

## 0.0.1.7(2024-02-21)

### Modify

Readme: Add new feature descriptions and demo images

### Feature

Theme: Reference block supports gradients and ICONS

### Preview

![](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/aabf8169a72e04912addb7a20d66e65adc87610f/assets/90091016/PixPin_2024-02-21_17-38-05.png)

![](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/aabf8169a72e04912addb7a20d66e65adc87610f/assets/90091016/PixPin_2024-02-21_17-39-47.png)

## 0.0.1.6(2024-02-20)

### Added

Test: Tests the web page

Theme: Added min.css and css format

### Feature

Format: 使用scss替换原先的css

## 0.0.1.5(2024-02-11)

Format: Replace the original css with scss

## 0.0.1.4(2024-02-11)

Readme: Add secondary development instructions based on the `night` theme

Test: Add `dark.md`to demonstrate the effect

## 0.0.1.3(2023-12-03)

Theme: The reference block supports multiple colors

## 0.0.1.2(2023-12-03)

Readme: Modify the instruction picture (failed, needs to be withdrawn)

## 0.0.1.1(2023-12-03)

Readme: Modify the wording of the personalization introduction

## 0.0.1.0(2023-12-03)

Readme: Modify the instruction picture (failed, needs to be withdrawn)

## 0.0.0.9(2023-10-01)

Readme: Modified usage introduction

## 0.0.0.8(2023-10-01)

Readme: language badge address to user-images.githubusercontent.com

Test: Deletes the demo web page

## 0.0.0.7(2023-03-01)

Theme: Code block selected for highlighting style modification

## 0.0.0.6(2022-11-22)

Readme: Badge address change

## 0.0.0.5(2022-11-06)

Badge: Change the color

Test: renames the file

## 0.0.0.4(2022-10-30)

### Feature

Badge: New increased color span tag

### Modify

- Readme: Adds a personalized description
- Theme: Change the highlight color of the code

## 0.0.0.3(2022-10-27)

### Added

- Test: Add dark and light test pages

### Modify

- Readme: Add light and dark colors to preview static images
- Style: Adds avocado to the new style
- Theme: Remove reactive code, remove horizontal styles, and add `h2` and `h3` focus styles

### Preview

![img](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/80ca1f8959b28b729f24db2418cb87cf437f3210/source/img/kRk07KGs7k.png)

![vBhqaHoHSb)](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/80ca1f8959b28b729f24db2418cb87cf437f3210/source/img/vBhqaHoHSb.png)

## 0.0.0.2(2022-10-27)

Readme: Language logo picture error path (a775e72)

## 0.0.0.1(2022-10-27)

### Added

- Test: tests the web page
- Readme: English and Chinese files

### Features

- Format: Write the CSS 3
- Module: Modularity Introduces styles
- Style: Supports both dark and light modes

### Preview

![Typora_4ypLiBtCav.gif (1366×728)](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/a560039a51394ade1c1f763287a837808620476a/source/img/Typora_4ypLiBtCav.gif)

![](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/a560039a51394ade1c1f763287a837808620476a/source/img/Typora_8WX7OnCe1V.gif)