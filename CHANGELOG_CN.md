# 更新日志

## 0.0.0.5.1(2025-02-15)

Readme: 添加changelog和demo

## 0.0.4.8-0.0.5.0

Readme: 修改介绍

### 特性

Theme: 圆角风格，添加标题3缓动动画

### 新增

Readme: 更换演示图片、标准化Readme文档

## 0.0.4.7(2024-12-05)

Theme: 使用Mac风格修改标题样式，h2色块改为渐变色

## 0.0.4.6(2024-11-30)

Format: 代码样式美化

Theme: 修改标题专注模式样式，添加动画过渡效果

## 0.0.4.5(2024-11-26)

Theme: 修复侧边栏颜色问题

## 0.0.4.4(2024-11-22)

Readme: 修改GitHub风格代码块类型

## 0.0.4.3(2024-11-22)

Readme: 使用GitHub风格代码块美化原先提示信息

Theme: 修改渐变色red

## 0.0.4.2(2024-10-07)

### 特性

Theme: Mac风格代码样式

Style: 渐变色色块更加丰富

### 预览

![](https://img.picgo.net/2024/10/07/PixPin_2024-10-07_10-52-467f14386bb44c147e.webp)

![](https://img.picgo.net/2024/10/07/PixPin_2024-10-07_11-06-58b1385e0e76a87061.webp)

## 0.0.4.1(2024-10-06)

Style: 更新版本号

## 0.0.4.0(2024-10-06)

Readme: 更新图片链接

## 0.0.3.9(2024-08-18)

Readme: 更新图片链接

## 0.0.3.8(2024-08-18)

Readme: 更新图片链接，方便外网查看

## 0.0.3.7(2024-08-18)

### 特性

Theme: 添加GitHub风格引用块

## 0.0.3.6(2024-08-18)

### 移除

Theme: 深色和浅色模式文件

Test: 教程文档

### 特性

Theme: 一个主题文件实现深色和浅色主题自动切换

Readme: 使用Mac 风格演示主题样式

### 预览

![](https://img.picgo.net/2024/08/18/PixPin_2024-08-18_17-20-320212612d5f2cd57c.png)

![](https://img.picgo.net/2024/08/18/PixPin_2024-08-18_17-17-044fd017cfb6a0a347.png)

![](https://img.picgo.net/2024/08/18/PixPin_2024-08-18_17-19-26d719e5755cc35953.png)

## 0.0.3.6(2024-08-18)

Theme: 公共渐变色去除方向、角度限制，引用块样式修改自定义图标位置

## 0.0.3.5(2024-08-16)

Theme: 添加公共渐变色、带图标引用条新样式、新的图标

Test: 演示引用条bar效果

## 0.0.3.4(2024-07-31)

Format: 样式文件重命名

## 0.0.3.3(2024-07-30)

Theme: 修改代码块背景和光标颜色

## 0.0.3.2(2024-07-30)

Format: 图标文件重命名

Test: 删除英文教程文件

## 0.0.3.1(2024-07-30)

### 修改

Format: 使用map函数选择颜色

Readme: 修改自定义代码介绍

## 0.0.3.0(2024-07-27)

Format: 拆分主题scss为light和dark两个文件，代码文件合并到相应模式文件

Theme: 修改代码块样式

## 0.0.2.9(2024-07-27)

Readme: 修改介绍页面使用的演示图片

## 0.0.2.8(2024-07-24)

Readme: 修改介绍页面

## 0.0.2.7(2024-05-27)

Readme: 弃用oss图片加载速度

## 0.0.2.6(2024-05-03)

Readme: 添加代码块示例使用的语言，使用oss加速图片加载速度

## 0.0.2.5(2024-05-03)

### 修改

Readme: 添加代码块示例，使用picgo图床提高图片加载速度

### 新增

Theme: 压缩代码样式形成code.min.css

### 特性

Theme: 引用块支持宽屏渐变色块、可变色图标

## 0.0.2.4(2024-05-03)

Readme: 修改介绍页面

## 0.0.2.3(2024-05-03)

Readme: 修改介绍页面

## 0.0.2.3(2024-05-01)

Readme: 修改介绍页面

Theme: 下划线样式修改

## 0.0.2.2(2024-02-25)

### 修改

Readme: 添加新特性介绍

### 新增

Test: 添加英文教程文档

Style: 新增`badge`徽章文件夹

### 特性

调整标题样式，添加专注模式下样式

## 0.0.2.1(2024-02-24)

Format: 使用公用样式修改引用块、徽章样式

## 0.0.2.0(2024-02-23)

Theme: 修改h2、h3、h5样式

## 0.0.1.9(2024-02-23)

Format: 使用@mixin和@include语法修改scss公用样式

## 0.0.1.8(2024-02-21)

Readme: 修改演示图片地址

## 0.0.1.7(2024-02-21)

### 修改

Readme: 添加新特性介绍以及演示图片

### 特性

Theme: 引用块支持渐变色、图标

### 预览

![](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/aabf8169a72e04912addb7a20d66e65adc87610f/assets/90091016/PixPin_2024-02-21_17-38-05.png)

![](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/aabf8169a72e04912addb7a20d66e65adc87610f/assets/90091016/PixPin_2024-02-21_17-39-47.png)

## 0.0.1.6(2024-02-20)

### 新增

Test: 测试网页

Theme: 新增min.css和css格式

### 特性

Format: 使用scss替换原先的css

## 0.0.1.5(2024-02-11)

Readme: 修改徽章

## 0.0.1.4(2024-02-11)

Readme: 添加基于`night`主题二次开发说明

Test: 添加`dark.md`演示具体效果

## 0.0.1.3(2023-12-03)

Theme: 引用块支持多种颜色

## 0.0.1.2(2023-12-03)

Readme: 修改使用说明图片(失败，需要撤回)

## 0.0.1.1(2023-12-03)

Readme: 修改个性化介绍的措辞

## 0.0.1.0(2023-12-03)

Readme: 添加演示图片(失败，需要撤回)

## 0.0.0.9(2023-10-01)

Readme: 修改使用方法介绍

## 0.0.0.8(2023-10-01)

Readme: 语言徽章地址换成user-images.githubusercontent.com

Test: 删除demo网页

## 0.0.0.7(2023-03-01)

Theme: 代码块选中高亮样式修改

## 0.0.0.6(2022-11-22)

Readme: 徽章地址修改

## 0.0.0.5(2022-11-06)

Badge: 修改颜色

Test: 文件重命名

## 0.0.0.4(2022-10-30)

### 特性

Badge: 新增多色span标签

### 修改

- Readme: 添加个性化说明
- Theme: 修改代码高亮颜色

## 0.0.0.3(2022-10-27)

### 新增

- Test: 添加深色和浅色测试网页

### 修改

- Readme: 添加浅色和深色预览静态图
- Style: 添加`avocado`新风格
- Theme: 移除响应式代码、去除横线样式、新增`h2`和`h3`专注样式

### 预览

![img](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/80ca1f8959b28b729f24db2418cb87cf437f3210/source/img/kRk07KGs7k.png)

![vBhqaHoHSb)](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/80ca1f8959b28b729f24db2418cb87cf437f3210/source/img/vBhqaHoHSb.png)

## 0.0.0.2(2022-10-27)

Readme: 语言logo图片错误路径(a775e72)

## 0.0.0.1(2022-10-27)

### 添加

- Test: 测试网页
- Readme: 中英文档

### 特性

- Format: CSS 3书写
- Module: 模块化引入各样式
- Style: 支持深色和浅色模式

### 预览

![Typora_4ypLiBtCav.gif (1366×728)](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/a560039a51394ade1c1f763287a837808620476a/source/img/Typora_4ypLiBtCav.gif)

![](https://gitee.com/passwordgloo/Typora-foresee-theme/raw/a560039a51394ade1c1f763287a837808620476a/source/img/Typora_8WX7OnCe1V.gif)